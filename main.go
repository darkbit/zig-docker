package main

import (
	"archive/tar"
	"io"
	"bytes"
	"errors"
	"crypto"
	"encoding/hex"
	"fmt"
	"strings"
	"os"

	"github.com/google/go-containerregistry/pkg/v1/remote"
	"github.com/google/go-containerregistry/pkg/v1/types"
	"github.com/google/go-containerregistry/pkg/v1/partial"
	"github.com/google/go-containerregistry/pkg/name"
	"github.com/google/go-containerregistry/pkg/v1/mutate"
	"github.com/google/go-containerregistry/pkg/v1"
	"github.com/google/go-containerregistry/pkg/authn"
	"github.com/ulikunitz/xz"
)

const BASE_IMAGE = "alpine"

// uncompressedLayer implements partial.UncompressedLayer from raw bytes.
type uncompressedLayer struct {
	diffID    v1.Hash
	mediaType types.MediaType
	content   []byte
}

// DiffID implements partial.UncompressedLayer
func (ul *uncompressedLayer) DiffID() (v1.Hash, error) {
	return ul.diffID, nil
}

// Uncompressed implements partial.UncompressedLayer
func (ul *uncompressedLayer) Uncompressed() (io.ReadCloser, error) {
	return io.NopCloser(bytes.NewBuffer(ul.content)), nil
}

// MediaType returns the media type of the layer
func (ul *uncompressedLayer) MediaType() (types.MediaType, error) {
	return ul.mediaType, nil
}

func main() {
	fmt.Println("hi go :)")
	desc, err := remote.Get(name.MustParseReference(BASE_IMAGE))
	if err != nil {
		panic(err)
	}
	img, err := desc.Image()
	if err != nil {
		panic(err)
	}
	id, err := img.Digest()
	if err != nil {
		panic(err)
	}
	fmt.Println("Got base image", BASE_IMAGE, "with digest", id.Hex)

	zigtarxz, err := downloadLatest()
	if err != nil {
		panic(err)
	}
	fmt.Println("Got zig linux")

	var buf bytes.Buffer
	hasher := crypto.SHA256.New()
	mw := io.MultiWriter(&buf, hasher)

	tw := tar.NewWriter(mw)
	zigtar, err := xz.NewReader(bytes.NewReader(zigtarxz))
	if err != nil {
		panic(err)
	}
	tr := tar.NewReader(zigtar)
	for {
		hdr, err := tr.Next()
		if err != nil {
			if errors.Is(err, io.EOF) {
				break
			}
			panic(err)
		}

		_, path, found := strings.Cut(hdr.Name, "/")
		if !found {
			continue
		}
		if suffix, found := strings.CutPrefix(path, "lib/"); found {
			hdr.Name = "usr/lib/zig/" + suffix
		} else if path == "zig" {
			hdr.Name = "usr/bin/zig"
		} else {
			continue
		}

		if err := tw.WriteHeader(hdr); err != nil {
			panic(err)
		}
		if _, err := io.Copy(tw, tr); err != nil {
			panic(err)
		}
	}
	
	if err := tw.Close(); err != nil {
		panic(err)
	}

	h := v1.Hash {
		Algorithm: "sha256",
		Hex: hex.EncodeToString(hasher.Sum(make([]byte, 0, hasher.Size()))),
	}

	l, err := partial.UncompressedToLayer(&uncompressedLayer{
		diffID: h,
		mediaType: types.OCILayerZStd,
		content: buf.Bytes(),
	})
	if err != nil {
		panic(err)
	}
	
	newimg, err := mutate.AppendLayers(img, l)
	if err != nil {
		panic(err)
	}

	imagename := os.Getenv("CI_REGISTRY_IMAGE")
	username := os.Getenv("CI_REGISTRY_USER")
	password := os.Getenv("CI_REGISTRY_PASSWORD")
	fmt.Println("Pushing image to", imagename, "with user", username)
	in, err := name.ParseReference(imagename)
	if err != nil {
		panic(err)
	}
	err = remote.Write(in, newimg, remote.WithAuth(&authn.Basic {
		Username: username,
		Password: password,
	}))
	if err != nil {
		panic(err)
	}
}
