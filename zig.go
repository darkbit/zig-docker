package main

import (
	"encoding/json"
	"net/http"
	"encoding/hex"
	"io"
	"fmt"
	"crypto"

	minisign "github.com/jedisct1/go-minisign"
)

const ZIG_MINISIGN_PUBKEY = "RWSGOq2NVecA2UPNdBUZykf1CCb147pkmdtYxgb3Ti+JO/wCYvhbAb/U"

type versionInfo struct {
	X64 data `json:"x86_64-linux"`
}

type data struct {
	Tarball string
	Shasum string
}

func downloadLatest() ([]byte, error) {
	resp, err := http.Get("https://ziglang.org/download/index.json")
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()
	versions := map[string]versionInfo{}
	if err := json.NewDecoder(resp.Body).Decode(&versions); err != nil {
		return nil, err
	}
	latest, ok := versions["master"]
	if !ok {
		return nil, fmt.Errorf("Couldn't find the master version")
	}
	
	tresp, err := http.Get(latest.X64.Tarball)
	if err != nil {
		return nil, err
	}
	defer tresp.Body.Close()
	tarfile, err := io.ReadAll(tresp.Body)
	if err != nil {
		return nil, err
	}

	h := crypto.SHA256.New()
	h.Write(tarfile)
	actual := hex.EncodeToString(h.Sum(make([]byte, 0, h.Size())))
	if actual != latest.X64.Shasum {
		return nil, fmt.Errorf("Expected Hash %s, but got %s", latest.X64.Shasum, actual)
	}

	sresp, err := http.Get(latest.X64.Tarball + ".minisig")
	if err != nil {
		return nil, err
	}
	defer sresp.Body.Close()
	sig, err := io.ReadAll(sresp.Body)
	if err != nil {
		return nil, err
	}
	signature, err := minisign.DecodeSignature(string(sig))
	if err != nil {
		return nil, err
	}

	pubkey, err := minisign.NewPublicKey(ZIG_MINISIGN_PUBKEY)
	if err != nil {
		return nil, err
	}
	_, err = pubkey.Verify(tarfile, signature)
	if err != nil {
		return nil, err
	}
	return tarfile, nil
}
